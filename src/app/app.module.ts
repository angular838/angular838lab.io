import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from './material/material.module';
import { NodeComponent } from './node/node.component';
import { NodeListComponent } from './node-list/node-list.component';
import { FilterUniquePipe } from './filter-unique.pipe';
import { FetchClientDetailComponent } from './fetch-client-detail/fetch-client-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NodeComponent,
    NodeListComponent,
    FilterUniquePipe,
    FetchClientDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
