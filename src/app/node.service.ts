import { Injectable, Inject, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Node } from './node';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class NodeService {
  constructor(private http: HttpClient) { }

  nodeUrl = '/assets/json/graph.json';
  baseUrl = '/assets/json/';

  get(): Observable<Node[]> {
    return this.http.get<Node[]>(this.nodeUrl)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  getNode(name: string): Observable<Node> {
    return this.http.get<Node>(this.getSpecificNodeUrl(this.baseUrl, name))
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  getSpecificNodeUrl(baseUrl: string, nodeName: string) {
    return baseUrl + nodeName + '.json';
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
