import { Component, OnInit } from '@angular/core';
import { NodeService } from '../node.service';

@Component({
  selector: 'app-node-list',
  templateUrl: './node-list.component.html',
  styleUrls: ['./node-list.component.sass']
})
export class NodeListComponent implements OnInit {

  constructor(
    private nodeService: NodeService
  ) { }

  ngOnInit() {
    this.nodeService.get().subscribe(nodes => {
      console.log(nodes);
    });
  }

}
