import { Component, OnInit } from '@angular/core';
import { TableBasicExample } from '../table-basic-example';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  tableBasicExample: TableBasicExample;
  displayedColumns = ['position', 'name', 'weight'];
  id: string;

  constructor(
    private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.tableBasicExample = new TableBasicExample();
    this.activatedRoute.params.subscribe(data => {
      this.id = data.id;
    });
  }

}
