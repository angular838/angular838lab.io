import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { TableBasicExample } from '../table-basic-example';
import { ActivatedRoute, Params } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { MatTableModule } from '@angular/material/table';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let params: BehaviorSubject<Params>;

  beforeEach(async(() => {
    params = new BehaviorSubject<Params>({});
    const fakeActivatedRoute = { params: of([{ item: 1 }])};
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
      ],
      imports: [
        MatTableModule
      ],
      providers: [
        TableBasicExample,
        {
          provide: ActivatedRoute,
          useValue: fakeActivatedRoute
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    component.tableBasicExample = new TableBasicExample();
    fixture.detectChanges();
  });

  it('should create', () => {
    console.log(component.tableBasicExample);
    expect(component).toBeTruthy();
  });
});
