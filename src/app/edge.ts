export class Edge {
    id: number;
    name: string;
    startNodeId: number;
    endNodeId: number;
}
