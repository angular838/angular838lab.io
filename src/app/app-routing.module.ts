import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NodeListComponent } from './node-list/node-list.component';
import { NodeComponent } from './node/node.component';
import { FetchClientDetailComponent } from './fetch-client-detail/fetch-client-detail.component';


const routes: Routes = [
  { path: '', component: FetchClientDetailComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
