export interface ClientDetail {
    id: number;
    uniqueIdentifier: string;
    name: string;
    legacyName: string;
    yearEstablished: string;
    streetAddress: string;
    city: string;
    state: string;
    accountManager: string;
    email: string;
  }
