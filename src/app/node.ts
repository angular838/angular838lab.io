import { Edge } from './edge';

export class Node {
    id: number;
    name: string;
    edges: Array<Edge>;
}
